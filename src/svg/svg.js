import { registerPainter } from '../quark-renderer';
import './graphic';
import SVGPainter from './SVGPainter';

registerPainter('svg', SVGPainter);
